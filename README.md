# js-hometask-events

## Ответьте на вопросы

### 1. Что такое событие? Какими способами можно назначить обработчик события на элемент?
> Ответ: Сигнал, который генерируется браузером на определенное действие пользователя, либо может быть сгенерирован программно.
```js
/*1. Вызвать функцию в html-атрибуте*/
<button value="Клик" onclick="click()" />
<script>
function click() {
    console.log('Клик!');
}
</script>

/*2. Присвоить функцию-обработчик свойству DOM-объекту*/
<button id="btn" value="Клик" />
<script>
  btn.onclick = function() {
    console.log('Клик!');
  };
</script>

/*3. Назначение обработчика события при помощи addEventListener (Есть возможность назначить на событие сразу несколько обработчиков)*/
<button id="btn" value="Жмак"/>


<script>
  btn.onclick = ()=> {console.log('first');}
  btn.addEventListener("click", ()=>{console.log('second');}));
  btn.addEventListener("click", ()=>{console.log('third');});
</script>

```

### 2. Что такое всплытие? Каким образом его можно остановить?
> Ответ: Всплытие - это когда событие срабатывает сначала на элементе, а потом на его родителях. Всплытие можно остановить при помощи метода stopPropagation() или stopImmediatePropagation(), различие этих методов заключается в том, что второй прекращает выполнение события сразу, а первый только для последующих и иерархии элементов. 

### 3. Каким образом можно получить детальную информацию о событии? Чем отличаются event.target и event.currentTarget?
> Ответ: Информацию о событии можно просмотреть в объекте события event. Свойство event.target указывает на первый элемент в цепочке всплытия, на котором сработало событие (неизменен), а event.currentTarget указывает на элемент, на котором было перехвачено событие во время всплытия.

### 4. Приведите пример действия браузера по умолчанию. Каким образом действие можно отменить?
> Ответ: Действие contextmenu, которое открывает на ПКМ контекстное меню браузера. Отменить действие по-умолчанию можно 2-я способами:

```js
/*1. Использовать метод preventDefault()*/
<div>
    <a id="myLink" href="https://pornhub.com/">PornHub</a>
</div>
<script>
    myLink.addEventListener('click', (event)=>{
        alert('Do you want some porn? Ok, dude, check it out');
        location.href = 'https://www.youtube.com/watch?v=-7n4t0cbVD4';
        event.preventDefault();
    });
</script>

/*2. Через on-событие вернуть false*/
<div>
    <a id="myLink" href="https://pornhub.com/">PornHub</a>
</div>
<script>
    myLink.onclick = function(event){
        alert('Do you want some porn? Ok, dude, check it out');
        location.href = 'https://www.youtube.com/watch?v=-7n4t0cbVD4';
        return false;
    };
</script>

```


## Выполните задания

* Допишите функции в tasks.js;
* Для проверки работы функций откройте index.html в браузере;
* Создайте Merge Request с решением.
