'use strict';

/*
  Вам необходимо добавить обработчики событий в соотвествии с заданием.
  Вам уже даны готовые стили и разметка. Менять их не нужно,
  за исключением навешивания обработчиков и добавления data - аттрибутов
*/

/**
 * Задание 1
 * Необходимо сделать выпадающее меню.
 * Меню должно открываться при клике на кнопку,
 * закрываться при клике на кнопку при клике по пункту меню
*/
menuBtn.addEventListener('click', ()=>{
  // document.getElementById('menu').className = 'menu-opened';
  document.getElementById('menu').classList.toggle('menu-opened');
});

const menu_item = document.getElementsByClassName('menu-item');
for (const item of menu_item) {
  item.addEventListener('click', ()=>{
    document.getElementById('menu').classList.remove('menu-opened');
  })
} 


/**
 * Задание 2
 * Необходимо реализовать перемещение блока по клику на поле.
 * Блок должен перемещаться в место клика.
 * При выполнении задания вам может помочь свойство element.getBoundingClientRect()
*/
field.addEventListener('click', (event)=>{
  let rect = document.getElementById('movedBlock');

  rect.style.left = `${event.clientX - field.getBoundingClientRect().x}px`;
  rect.style.top = `${event.clientY - field.getBoundingClientRect().y}px`;
});

/**
 * Задание 3
 * Необходимо реализовать скрытие сообщения при клике на крестик при помощи делегирования
*/
messager.addEventListener('click', (event)=>{
  let {target} = event;
  let msg = target.parentElement;
  msg.style.display = 'none'
});


/**
 * Задание 4
 * Необходимо реализовать вывод сообщения при клике на ссылку.
 * В сообщении спрашивать, что пользователь точно хочет перейти по указанной ссылке.
 * В зависимости от ответа редиректить или не редиректить пользователя
*/
links.addEventListener('click', (event)=>{
  let {target} = event;
  if(confirm("Do you want to go there?")) {
    location.href = target.href;
  }
  event.preventDefault();
});


/**
 * Задание 5
 * Необходимо сделать так, чтобы значение заголовка изменялось в соответствии с измением
 * значения в input
*/
fieldHeader.addEventListener('keyup', (event)=>{
  let neighbor = event.currentTarget.previousElementSibling;
  neighbor.innerText = event.currentTarget.value;
});